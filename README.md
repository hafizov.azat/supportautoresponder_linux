## Текущая версия программы 1.4 ##

## Инструкция по настройке и запуску программы ##

- Находим телеграм бота [parser_feedbacks_bot](https://t.me/parser_feedbacks_bot) и нажать **“Запустить”** или отправить команду **/start** чтобы бот имел право отправлять уведомление уведомления на Ваш телеграм аккаунт. После чего бот Вам вышлет Ваш telegram id, он нам понадобится дальше...

![image](/instructions/img/tg_start.JPG)

Так нужно сделать со всех телеграм аакаунтов по которым хотите получать уведомления. Нужно собрать списко всех telegram id да дальнейшей настройки

- Подключаемся к серверу через **ssh**

- Переходим в домашний каталог

>> `cd ~/`

- Сразу получем root продавца

>> `sudo -s`

- Скачиваем актуальную версию с [Gitlab](https://gitlab.com/hafizov.azat/supportautoresponder_linux/) (`apt install wget` если нет wget)

>> `wget https://gitlab.com/hafizov.azat/supportautoresponder_linux/-/archive/main/supportautoresponder_linux-main.zip`

- Распаковываем скаченный архив (`apt install unzip` если нет unzip)

>> `unzip supportautoresponder_linux-main.zip -d wb_supportautoresponder`

- Удаляем архив (если захотите, чтобы не мусорить)

>> `rm supportautoresponder_linux-main.zip`

- Переходим в директорию

>> `cd wb_supportautoresponder/supportautoresponder_linux-main/`

- Скачиваем WinSCP подключаемся к серверу, проходим в директорию с программой и скачиваем файл **Обращения.xlsx**

![image](/instructions/img/exel_upload.JPG)

- Открыть Exel файл **Обращения.xlsx** и заполнить данные в странице **«Телеграм»** полученными от бота telegram id. В столбе **«Телеграм ID пользователей которым приходят уведомления»** нужно указать id telegram пользователей, которые будут получать уведомления о поданных заявках, и результатах. В столбе **«Телеграм ID пользователей которым приходят ошибки»**  нужно указать  id telegram пользователей, которые будут получать сообщения об ошибках, которые могут возникнуть. Так же вы можете создать своего бота через [BotFather](https://t.me/BotFather) и вписать его **bot api** ключ если вы хотите получать уведомление в него.

![image](/instructions/img/tg_notifset.JPG)

- Можно заполнить данные в листе **«Первое обращение»** для рандомайзера сообщений. В первом столбе нужно указать приветственное слово, во втором проблему и так далее. Как это работает, программа берет случайный первый столб, прибавляет случайный второй, прибавляет случайный третий и прибавляет случайный четвертый, так получается случайное сообщение, данные в столбы можно заносить без ограничений.

![image](/instructions/img/one_msg.JPG)

- Можно и по такому принцепу поправить и второе обращение.

![image](/instructions/img/two_msg.JPG)

- Возвращаемся на сервер по ssh в каталог с программой. Даем права на запуск программам 

>> `chmod a+x feedbacks_parser.bin`

>> `chmod a+x feedbacks_parser_two.bin`

>> `chmod a+x feedbacks_monitor.bin`

>> `chmod a+x support_monitor.bin`

>> `chmod a+x services_control.bin`

>> `chmod a+x initialization.bin`

- Запускаем программу **initialization.bin** 

>> `./initialization.bin`

- Нужно ввести номер телефона с **7** на которую завязана ЛК в ВБ.

![image](/instructions/img/enter_phone.JPG)

- Далее нужно ввести **код** с **смс** или с **клиентского приложения** (чаще оно приходит туда), если ничего не пришло, закройте программу и откройте заново через 5 минут.

![image](/instructions/img/enter_code.JPG)

- Если по данному номеру в ЛК 2 или более магазина программа запросит выбрать какой по порядку выбрать магазин, нужно ввести **цифру** по порядку. Если выбрать _1_ в данном случаее выбирится _Смей Людмила Геннадиевна_, если 2 то второй вариант.

![image](/instructions/img/select_suplier.JPG)

- Далее нужно ввести **серийный ключ** который Вам выслали после преобретения программы. Серийный ключ привязывается к отдельному магазину. **один магазин** = **один серийный ключ**.

![image](/instructions/img/enter_key.JPG)

- Далее сервер лицензий ответит, если верификация пройдена то программа создаст службы **WB_feedbacks_parser.service**, **WB_feedbacks_parser_two.service**, **WB_feedbacks_monitor.service**, **WB_support_monitor.service** и **WB_services_control.service**.

- Проверяем статус служб, и убеждаемся что все слжбы работаю

>> `systemctl status WB_feedbacks_parser.service`

>> `systemctl status WB_feedbacks_parser_two.service`

>> `systemctl status WB_feedbacks_monitor.service`

>> `systemctl status WB_support_monitor.service`

>> `systemctl status WB_services_control.service`

**Настройка программы окночена!**

## Инструкция по авторизации в ЛК продавца ##

- В случае разлогиневании сессии в ЛК продавца бот вышлет соответсвующее сообщение.

![image](/instructions/img/dislogin.JPG)

- Подключаемся к серверу через ssh. Проходим в каталог с программой на сервере и запускаем **initialization.bin** и проходим инициализацию.

>> `./initialization.bin`

- Нужно ввести номер телефона с **7** на которую завязана ЛК в ВБ.

![image](/instructions/img/enter_phone.JPG)

- Далее нужно ввести **код** с **смс** или с **клиентского приложения** (чаще оно приходит туда), если ничего не пришло, закройте программу и откройте заново через 5 минут.

![image](/instructions/img/enter_code.JPG)

- Если по данному номеру в ЛК 2 или более магазина программа запросит выбрать какой по порядку выбрать магазин, нужно ввести **цифру** по порядку. Если выбрать _1_ в данном случаее выбирится _Смей Людмила Геннадиевна_, если 2 то второй вариант. По каждому магазину придется пройти инициализацию повторно.

![image](/instructions/img/select_suplier.JPG)

- Далее нужно ввести **серийный ключ** который Вам выслали после преобретения программы. Серийный ключ привязывается к к отдельному магазину. **один магазин** = **один серийный ключ**.

![image](/instructions/img/enter_key.JPG)

- Далее сервер лицензий ответит, если верификация пройдена программа завершить работу

**Авторизация в ЛК продавца окончена!**

## Инструкция по обновлению ##

Скачеваем актуальные репозитории с [GitLab](https://gitlab.com/hafizov.azat/supportautoresponder_linux/-/archive/main/supportautoresponder_linux-main.zip) на локальный компьютер и разархивируем.

Подключаемся к серверу по [WinSCP](https://winscp.net/eng/downloads.php) и закидываем **6** файла (**feedbacks_parser.bin**, **feedbacks_parser_two.bin**,  **feedbacks_monitor.bin**, **support_monitor.bin**, **services_control.bin** и **initialization.bin**) с скачанного архива на сервер с программой

![image](/instructions/img/update_bin.JPG)

- Подключаемся к серверу по ssh, в каталог с программой. Даем права на запуск программам 

>> `chmod a+x feedbacks_parser.bin`

>> `chmod a+x feedbacks_parser_two.bin`

>> `chmod a+x feedbacks_monitor.bin`

>> `chmod a+x support_monitor.bin`

>> `chmod a+x services_control.bin`

>> `chmod a+x initialization.bin`

- Далее перезапускаем службы

>> `systemctl restart WB_feedbacks_parser.service`

>> `systemctl restart WB_feedbacks_parser_two.service`

>> `systemctl restart WB_feedbacks_monitor.service`

>> `systemctl restart WB_support_monitor.service`

>> `systemctl restart WB_services_control.service`

**Обнавление программы произведен!**

## Как добавить несколько магазинов ##

- Подключаемся к серверу по ssh. Проходим в папку с программой и запускаем программу **initialization.bin** и пройти инициализацию.

>> `sudo ./initialization.bin`

- Нужно ввести номер телефона с **7** на которую завязана ЛК в ВБ.

![image](/instructions/img/enter_phone.JPG)

- Далее нужно ввести **код** с **смс** или с **клиентского приложения** (чаще оно приходит туда), если ничего не пришло, закройте программу и откройте заново через 5 минут.

![image](/instructions/img/enter_code.JPG)

- Если по данному номеру в ЛК 2 или более магазина программа запросит выбрать какой по порядку выбрать магазин, нужно ввести **цифру** по порядку того магазина которую Вы хотите добавить.

![image](/instructions/img/select_suplier.JPG)

- Далее нужно ввести **серийный ключ** который Вам выслали после преобретения программы. Серийный ключ привязывается к отдельному магазину. **один магазин** = **один серийный ключ**.

![image](/instructions/img/enter_key.JPG)

- Далее сервер лицензий ответит, если верификация пройдена программа выдаст соответсвующеесообщение и завершит работу. 

**Добавление магазина произведен!**

